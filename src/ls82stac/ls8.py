import os
import csv
import gzip
from io import StringIO
from urllib.request import urlopen
from dateutil.parser import parse
from shapely import geometry
from pystac.extensions.eo import Band
import pystac

def get_scenes(scene_list):

    scenes = None
    
    if scene_list is not None:
        
        if os.path.exists(scene_list):
            print('read from file')
            #with open(scene_list, 'r') as file:
            #    content = file.read()
            
            scenes = list(csv.DictReader(open(scene_list)))
    
    else:
        
        # Collection 1 data from URL
        url = 'https://landsat-pds.s3.amazonaws.com/c1/L8/scene_list.gz'

        # Read and unzip the content
        response = urlopen(url)
        gunzip_response = gzip.GzipFile(fileobj=response)
        content = gunzip_response.read()

        # Read the scenes in as dictionaries
        scenes = list(csv.DictReader(StringIO(content.decode("utf-8"))))

    return scenes

class LS8_scene(): 

    def __init__(self, scene):

        self.scene = scene

        self.mtl_url = self.get_asset_url('MTL.txt')
        
        self.metadata = self.get_metadata(self.mtl_url)

        #self.item = self.get_item()

        self.landsat_band_info = {
            'B1': {
                'band': pystac.extensions.eo.Band.create(name="B1", common_name="coastal", center_wavelength=0.48, full_width_half_max=0.02),
                'gsd': 30.0
            },
            'B2': {
                'band': Band.create(name="B2", common_name="blue", center_wavelength=0.44, full_width_half_max=0.06),
                'gsd': 30.0
            },
            'B3': {
                'band': Band.create(name="B3", common_name="green", center_wavelength=0.56, full_width_half_max=0.06),
                'gsd': 30.0
            },
            'B4': {
                'band': Band.create(name="B4", common_name="red", center_wavelength=0.65, full_width_half_max=0.04),
                'gsd': 30.0
            },
            'B5': {
                'band': Band.create(name="B5", common_name="nir", center_wavelength=0.86, full_width_half_max=0.03),
                'gsd': 30.0
            },
            'B6': {
                'band': Band.create(name="B6", common_name="swir16", center_wavelength=1.6, full_width_half_max=0.08),
                'gsd': 30.0
            },
            'B7': {
                'band': Band.create(name="B7", common_name="swir22", center_wavelength=2.2, full_width_half_max=0.2),
                'gsd': 30.0
            },
            'B8': {
                'band': Band.create(name="B8", common_name="pan", center_wavelength=0.59, full_width_half_max=0.18),
                'gsd': 15.0
            },
            'B9': {
                'band': Band.create(name="B9", common_name="cirrus", center_wavelength=1.37, full_width_half_max=0.02),
                'gsd': 30.0
            },
            'B10': {
                'band': Band.create(name="B10", common_name="lwir11", center_wavelength=10.9, full_width_half_max=0.8),
                'gsd': 100.0
            },
            'B11': {
                'band': Band.create(name="B11", common_name="lwir12", center_wavelength=12.0, full_width_half_max=1.0),
                'gsd': 100.0
            }
    }

    def get_asset_url(self, suffix):

        product_id = self.scene['productId']
        download_url = self.scene['download_url']    
        asset_filename = '{}_{}'.format(product_id, suffix)
        
        return download_url.replace('index.html', asset_filename)

    def get_metadata(self, url):
        """ Convert Landsat MTL file to dictionary of metadata values """
        
        mtl = {}
        mtl_text = pystac.STAC_IO.read_text(url)
        
        for line in mtl_text.split('\n'):
            
            meta = line.replace('\"', "").strip().split('=')
            
            if len(meta) > 1:
                key = meta[0].strip()
                item = meta[1].strip()
                if key != "GROUP" and key != "END_GROUP":
                    mtl[key] = item
        
        return mtl


    def get_scene_id(self):

        return self.metadata['LANDSAT_SCENE_ID'][:-5]


    def get_datetime(self):

        return parse('{}T{}'.format(self.metadata['DATE_ACQUIRED'], self.metadata['SCENE_CENTER_TIME']))

    def get_bbox(self):

        coords = [geometry.Point(float(self.metadata['CORNER_UL_LON_PRODUCT']), 
                                float(self.metadata['CORNER_UL_LAT_PRODUCT'])),
                  geometry.Point(float(self.metadata['CORNER_UR_LON_PRODUCT']), 
                                float(self.metadata['CORNER_UR_LAT_PRODUCT'])),
                  geometry.Point(float(self.metadata['CORNER_LR_LON_PRODUCT']), 
                                 float(self.metadata['CORNER_LR_LAT_PRODUCT'])),
                  geometry.Point(float(self.metadata['CORNER_LL_LON_PRODUCT']), 
                                 float(self.metadata['CORNER_LL_LAT_PRODUCT'])),
                  geometry.Point(float(self.metadata['CORNER_UL_LON_PRODUCT']), 
                                 float(self.metadata['CORNER_UL_LAT_PRODUCT']))]

        return list(geometry.Polygon([[p.x, p.y] for p in coords]).bounds)

    def get_geometry(self):

        bbox = self.get_bbox()

        url = self.get_asset_url('ANG.txt')
        sz = []
        coords = []
        ang_text = pystac.STAC_IO.read_text(url)
        for line in ang_text.split('\n'):
            if 'BAND01_NUM_L1T_LINES' in line or 'BAND01_NUM_L1T_SAMPS' in line:
                sz.append(float(line.split('=')[1]))
            if 'BAND01_L1T_IMAGE_CORNER_LINES' in line or 'BAND01_L1T_IMAGE_CORNER_SAMPS' in line:
                coords.append([float(l) for l in line.split('=')[1].strip().strip('()').split(',')])
            if len(coords) == 2:
                break
        dlon = bbox[2] - bbox[0]
        dlat = bbox[3] - bbox[1]
        lons = [c/sz[1] * dlon + bbox[0] for c in coords[1]]
        lats = [((sz[0] - c)/sz[0]) * dlat + bbox[1] for c in coords[0]]
        coordinates = [[
            [lons[0], lats[0]], [lons[1], lats[1]], [lons[2], lats[2]], [lons[3], lats[3]], [lons[0], lats[0]]
        ]]

        return {'type': 'Polygon', 'coordinates': coordinates}

    def get_cloud_cover(self):

        return float(self.metadata['CLOUD_COVER'])

    def get_other_assets(self):
        return {
        'thumbnail': {
            'href': self.get_asset_url('thumb_large.jpg'),
            'media_type': pystac.MediaType.JPEG
        },
        'index': {
            'href': self.get_asset_url('index.html'),
            'media_type': 'application/html'
        },
        'ANG': {
            'href': self.get_asset_url('ANG.txt'),
            'media_type': 'text/plain'
        },
        'MTL': {
            'href': self.get_asset_url('MTL.txt'),
            'media_type': 'text/plain'
        },
        'BQA': {
            'href': self.get_asset_url('BQA.TIF'),
            'media_type': pystac.MediaType.GEOTIFF
        }
    }

    def _add_assets(self, item):
        # Add bands
        for band_id, band_info in self.landsat_band_info.items():

            band_url = self.get_asset_url('{}.TIF'.format(band_id))

            asset = pystac.Asset(href=band_url, 
                                 media_type=pystac.MediaType.COG)

            bands = [band_info['band']]

            item.ext.eo.set_bands(bands, asset)

            item.add_asset(band_id, asset)

            # If this asset has a different GSD than the item, set it on the asset
            if band_info['gsd'] != item.common_metadata.gsd:

                item.common_metadata.set_gsd(band_info['gsd'], 
                                                  asset)

        # Add other assets
        for asset_id, asset_info in self.get_other_assets().items():
            item.add_asset(asset_id, 
                               pystac.Asset(href=asset_info['href'], 
                                            media_type=asset_info['media_type']))

    def get_epsg(self):

        bbox = self.get_bbox()

        min_lat = bbox[1]
        max_lat = bbox[3]

        if 'UTM_ZONE' in self.metadata:
            center_lat = (min_lat + max_lat)/2.0
            return int(('326' if center_lat > 0 else '327') + self.metadata['UTM_ZONE'])
        else:
            return None

    def get_view_info(self):

        return { 'sun_azimuth': float(self.metadata['SUN_AZIMUTH']),
                 'sun_elevation': float(self.metadata['SUN_ELEVATION']) }

    def get_item(self):

        item_id = self.get_scene_id()
        item_datetime = self.get_datetime()
        item_geometry = self.get_geometry()
        item_bbox = self.get_bbox()

        item_properties = {}
        
        item_properties['mission'] = 'landsat-8'
        item_properties['platform'] = 'landsat-8'
        item_properties['instruments'] = ['oli', 'tirs']
                
        item = pystac.Item(id=item_id, 
                           datetime=item_datetime,
                           geometry=item_geometry,
                           bbox=item_bbox,
                           properties=item_properties,
                           collection='landsat-8-l1')

        
        item.common_metadata.gsd = 30.0

        item.ext.enable('eo')
        item.ext.enable('projection')
        item.ext.enable('view')

        item.ext.eo.cloud_cover = self.get_cloud_cover()

        item.ext.projection.epsg = self.get_epsg() 

        view_info = self.get_view_info()

        item.ext.view.sun_azimuth = view_info['sun_azimuth']
        item.ext.view.sun_elevation = view_info['sun_elevation']    

        self._add_assets(item)

        eo_item = pystac.extensions.eo.EOItemExt(item)
        #[landsat_band_info[x]['band'] for x in landsat_band_info.keys()]
        eo_item.apply([self.landsat_band_info[x]['band'] for x in self.landsat_band_info.keys()])
        eo_item.set_bands([self.landsat_band_info[x]['band'] for x in self.landsat_band_info.keys()])
        
        return item