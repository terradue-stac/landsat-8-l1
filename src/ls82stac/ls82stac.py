import os
import click
import sys
import logging 
from pystac import Catalog, STAC_IO, CatalogType, read_file, Link, write_file, LinkType
from urllib.parse import urlparse
from .ls8 import LS8_scene, get_scenes

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

def keep_scene(scene, path, row, date=None):
    
    is_path_row = scene['path'] in str(path) and scene['row'] in str(row)
     
    is_correct_date = True
    
    if date is not None:
    
        is_correct_date = date in scene['acquisitionDate']
    
    return is_path_row and is_correct_date


def get_item_subpath(path, row):
    
    sub_path = '/'.join([path, row])
    
    return sub_path 

def fix_catalog_children(cat_path, rel='child'):
    
    print('cat_path is ', cat_path)
    base_url = 'https://terradue-stac.gitlab.io/landsat-8-l1'

    # if the catalog exits, read it, else create a new one
    try:
        
        cat = Catalog.from_file(os.path.join(cat_path, 'catalog.json'))
        
        # remove any item the catalog may have if not adding an item
        if rel=='child': 
            cat.remove_links(rel='item')
        
    except FileNotFoundError:
        
        cat = Catalog(id=os.path.basename(cat_path),
                      description=os.path.basename(cat_path)) 
        
        cat.set_self_href(os.path.join(base_url, 
                                       cat_path.replace('./', ''),
                                       'catalog.json'))
        
        # only add a parent when needed
        if cat_path not in ['./']:
            #print(cat_path.replace('.', '').split('/')[:-1])
            if cat_path.replace('.', '').split('/')[:-1] == ['']:

                if cat.get_parent() is None:
                    cat.add_link(Link(rel='parent', target='/'.join([base_url, 
                                                                 'catalog.json'])))

            else:
                if cat.get_parent() is None:
                    cat.add_link(Link(rel='parent', target='/'.join([base_url, 
                                                                 *cat_path.replace('./', '').split('/')[:-1], 
                                                                 'catalog.json'])))
    
    rel_links = cat.get_links(rel=rel)

    if len(rel_links) == 0:
    
        for f in ['{}/{}/catalog.json'.format(base_url, f.path.replace('./', '')) for f in os.scandir(cat_path) if f.is_dir()]:
            print(f)
            if '.ipynb_checkpoints' in f: 
                continue
            
            if rel=='child':
                target = f
            elif rel=='item':
                item_name = os.path.dirname(urlparse(f)[2]).split('/')[-1]
                target = f.replace('catalog.json', '{0}.json'.format(item_name))
                print(item_name, target)
                #print('target ', target)
            else:
                continue
            print('adding rel {} {}'.format(rel, target))   
            rel_link = Link(rel=rel, target=target)
            cat.add_link(rel_link)
            
    else:
        
        if rel=='child':
            
            rel_on_fs = ['{}/{}/catalog.json'.format(base_url, 
                                                     f.path.replace('./', '')) for f in os.scandir(cat_path) if f.is_dir()]

            rel_in_stac = [link.get_href() for link in rel_links]
        
        if rel=='item':
            
            print([f.path for f in os.scandir(cat_path) if f.is_dir()])
            
            rel_on_fs = ['{}/{}/{}.json'.format(base_url,
                                                f.path.replace('./', ''),
                                                os.path.basename(urlparse(f.path)[2]).split('/')[-1]) for f in os.scandir(cat_path) if f.is_dir()]
            print(rel_on_fs)
            rel_in_stac = [link.get_href() for link in rel_links]
            print(rel_in_stac)
            
        for missing_element in list((set(rel_on_fs).difference(rel_in_stac))):
            
            rel_link = Link(rel=rel, target=missing_element)
            cat.add_link(rel_link)

                
    # write catalog
    dest_href=os.path.join(cat_path, 'catalog.json')
    write_file(cat, dest_href=dest_href)

@click.command()
@click.option('--target', '-t', 'target_dir', default='public', help='target directory')
@click.option('--path', '-p', 'path', help='Landsat-8 path')
@click.option('--row', '-r', 'row', help='Landsat-8 row')
@click.option('--date-of-interest', '-d', 'date_of_interest', default=None, help='date 2017-04-27')
@click.option('--aws-scenes', '-a', 'scene_list', default=None, help='local path to downloaded https://landsat-pds.s3.amazonaws.com/c1/L8/scene_list.gz')
def main(target_dir, path, row, date_of_interest, scene_list):

    
    
def calibrate()
    
    path = str(path).zfill(3)
    row = str(row).zfill(3)
    
    if not os.path.exists(target_dir):
    
        os.mkdir(target_dir)
    
    logging.info('Getting scenes from AWS')
    scenes = get_scenes(scene_list)
    
    logging.info('Filter scenes')
    filtered_scenes = [scene for scene in scenes if keep_scene(scene, path, row, date_of_interest)]
    
    logging.info('Scenes to STAC catalog')
    
    if (filtered_scenes):
        
        for scene in filtered_scenes:
            
            ls = LS8_scene(scene)
            
            item = ls.get_item()
            
            print(item)
            
            sub_path = get_item_subpath(path, row)
            
            # check if the item folder tree is already created
            if not os.path.exists(os.path.join(target_dir, sub_path, item.id)):

                print('Adding item {}'.format(item.id))

                try:
                    os.makedirs(os.path.join(target_dir, 
                                             sub_path))

                except FileExistsError:
                    pass

                write_file(item, 
                           dest_href=os.path.join(target_dir, 
                                                  sub_path, 
                                                  item.id, 
                                                  '{}.json'.format(item.id)))
            
    os.chdir(target_dir) 
    # walk through the tree and fix the catalog.json along the way
    for x in os.walk('./'):

        if len(x[0].split('/')) < 3:
            
            # got a child 
            fix_catalog_children(x[0])
        
        elif len(x[0].split('/'))==3: 
            
            # got to an item
            fix_catalog_children(x[0], rel='item')
            
    return True


if __name__ == "__main__":
    main()